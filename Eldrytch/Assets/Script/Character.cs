using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Character : MonoBehaviour, IDeselectHandler
{
    public int iCharID;

    public Button bCharbutton;

    public int izoneAssigned = -1;
    public int isanity;
    public int[,] irelationship;
    public int ideathMark;

    private bool bFarm;

    public Text debugText;

    // Start is called before the first frame update
    void Start()
    {
        bCharbutton.onClick.AddListener(buttonSelected);
    }

    
    // Update is called once per frame
    void Update()
    {
        
    }

    public void damageSanity(int sanityDamage)
    {
        isanity = isanity - (1 * sanityDamage);
    }
    public void zoneSwitch(int zone, bool farm)
    {
        izoneAssigned = zone;

        if (farm)
        { bFarm = true; }
        else
        { bFarm = false; }

    }
    public void buttonSelected()
    {

    }
    public void OnDeselect(BaseEventData data)
    {
        
        
        debugText.text = "Zone: " + izoneAssigned + " Sanity: " + isanity;
    }

}
