using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Main : MonoBehaviour // IPointerDownHandler, IPointerUpHandler
{
    private int iresourceX = 10;
    private int iresourceY = 5;
    private int iresourceZ = 121;
    private int iresourceW = 18;

    public Text txtresourceX;
    public Text txtresourceY;

    public Button goNextDay;

    public Button bZone1;

    public Zone zone1;

    [SerializeField]
    private Character[] characters;
    [SerializeField]
    private Zone[] allZones;

    private GameObject prevSelected;

    // Start is called before the first frame update
    void Start()
    {
        goNextDay.onClick.AddListener(zoneNextDay);

        bZone1.onClick.AddListener(delegate { bZoneSelect(); });

    }

    // Update is called once per frame
    void Update()
    {
        txtresourceX.text = "CANDLE : " + iresourceX;
        txtresourceY.text = "BLOOD : " + iresourceY;

        GetLastGameObjectSelected();
    }
    private void GetLastGameObjectSelected()
    {
        if (EventSystem.current.currentSelectedGameObject != prevSelected)
        {
            if (prevSelected.tag == "CharacterButton" && EventSystem.current.currentSelectedGameObject.tag == "Zone")
            {
                //Do shit
            }

            prevSelected = EventSystem.current.currentSelectedGameObject;
        }
    }
    void bZoneSelect()
    {
        zone1.zoneNextDay(ref iresourceX, ref iresourceY, ref iresourceZ, ref iresourceW);
    }
    void zoneNextDay()
    {
        //Placeholder
        allZones = FindObjectsOfType<Zone>();

        foreach (Zone z in allZones)
        {
            z.zoneNextDay(ref iresourceX, ref iresourceY, ref iresourceZ, ref iresourceW);
        }

        characters = FindObjectsOfType<Character>();

        foreach (Character c in characters)
        {
            c.damageSanity(12);
            if (c.isanity <= 0)
            {

            }
        }
    }

}
