using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Zone : MonoBehaviour, ISelectHandler
{
    public int izoneid;

    public int icommonResourceidrate;
    public int iuncommonResourceidrate;
    public int irareResourceidrate;
    public int iepicResourceidrate;

    public GameObject zoneObject;
    public Text debugText;

    private int icharacterCount = 0;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        debugText.text = "Zone " + izoneid + " Count : " + icharacterCount +
            "\n Resource 1 " + icommonResourceidrate +
            "\n Resource 2 " + iuncommonResourceidrate +
            "\n Resource 3 " + irareResourceidrate +
            "\n Resource 4 " + iepicResourceidrate;
    }
   
    public void addCharCount()
    {
        icharacterCount++;
    }
    public void subCharCount()
    {
        icharacterCount--;
    }
    public void OnSelect(BaseEventData data)
    {
        
    }
    public void zoneNextDay(ref int icommonResource, ref int iuncommonResource, ref int irareResource, ref int iepicResource)
    {
        //Needs revision
        icommonResource = icommonResource + ((1 + icommonResourceidrate) * 7);
        iuncommonResource = iuncommonResource + ((1 + iuncommonResourceidrate) * 5);
        irareResource = irareResource + ((1 + irareResourceidrate) * 3);
        iepicResource = iepicResource + ((1 + iepicResourceidrate) * 1);
    }
}
